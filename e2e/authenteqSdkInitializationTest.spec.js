describe('Authenteq SDK Initialization', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Should initialize Authenteq SDK after starting identification', async () => {    
    await element(by.label('Identification')).tap();
    await waitFor(element(by.label('Start Identification')))
    .toBeVisible()
    .withTimeout(5000);    
    await element(by.label('Start Identification')).tap();
    await expect(element(by.label('Take a Photo')).atIndex(0)).toBeVisible();
  });
});
