//
//  ReactNativeAuthenteqFlow.swift
//  
//
//  Created by Vitaliy Gozhenko on 9/23/19.
//

import Foundation
import AuthenteqFlow

@objc(RNAuthenteqFlow)
class RNAuthenteqFlow: NSObject {

  var resolverBlock: RCTPromiseResolveBlock?
  var rejecterBlock: RCTPromiseRejectBlock?
  var authenteqViewController: UIViewController?

  @objc func identification(
    _ license: String,
    documents: [[String]],
    resolver resolve: @escaping RCTPromiseResolveBlock,
    rejecter reject: @escaping RCTPromiseRejectBlock
    ) {
    resolverBlock = resolve
    rejecterBlock = reject
    let documents: [[AuthenteqIdDocumentType]] = documents.map { $0.compactMap {
      switch $0 {
        case "PP": return .passport
        case "DL": return .driversLicense
        case "NID": return .idCard
      default: return nil
      }
    }}
    DispatchQueue.main.async {
      let viewController = AuthenteqFlow.instance.identificationViewController(
        with: license,
        documents: documents,
        delegate: self
      )
      viewController.modalPresentationStyle = .fullScreen
      UIApplication.shared.keyWindow?.rootViewController?.present(viewController, animated: true)
      self.authenteqViewController = viewController
    }
  }

  @objc func selfieAuthentication(
    _ license: String,
    userId: String,
    resolver resolve: @escaping RCTPromiseResolveBlock,
    rejecter reject: @escaping RCTPromiseRejectBlock
  ) {
    resolverBlock = resolve
    rejecterBlock = reject
    DispatchQueue.main.async {
      let viewController = AuthenteqFlow.instance.selfieAuthenticationViewController(
        with: license,
        userId: userId,
        delegate: self
      )
      viewController.modalPresentationStyle = .fullScreen
      UIApplication.shared.keyWindow?.rootViewController?.present(viewController, animated: true)
      self.authenteqViewController = viewController
    }
  }

  @objc func getVersion(_ callback: RCTResponseSenderBlock) {
    let version = Bundle(for: AuthenteqFlow.self).infoDictionary?["CFBundleShortVersionString"] as? String ?? "?"
    callback([version])
  }


  func toDictionary(result: IdentificationResult) -> [String: Any?] {
    var dictionary = [String: Any]()
    dictionary["userId"] = result.userId
    dictionary["selfieImageFile"] = result.selfieImageFilePath
    dictionary["documents"] = result.documents.map({ toDictionary(document: $0) })
    return dictionary
  }
  
  func toDictionary(document: DocumentIdentificationResult) -> [String: Any?] {
    switch document {
    case let document as PassportIdentificationResult:
      return toDictionary(passport: document)
    case let document as IdCardIdentificationResult:
      return toDictionary(idCard: document)
    case let document as DriversLicenseIdentificationResult:
      return toDictionary(driversLicense: document)
    default:
      return [:]
    }
  }
  
  func toDictionary(passport document: PassportIdentificationResult) -> [String: Any?] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var dictionary = [String: Any]()
    dictionary["documentFrontFile"] = document.documentImageFilePath
    dictionary["documentNumber"] = document.documentNumber
    dictionary["documentType"] = document.documentType
    dictionary["givenNames"] = document.givenNames
    dictionary["isEighteenPlus"] = document.isEighteenPlus?.boolValue
    dictionary["isSixteenPlus"] = document.isSixteenPlus?.boolValue
    dictionary["isTwentyOnePlus"] = document.isTwentyOnePlus?.boolValue
    dictionary["issuingCountry"] = document.issuingCountry
    dictionary["nationality"] = document.nationality
    dictionary["sex"] = document.sex
    dictionary["surname"] = document.surname
    dictionary["dateOfBirth"] = dateFormatter.string(fromOptional: document.dateOfBirth)
    dictionary["dateOfExpiry"] = dateFormatter.string(fromOptional: document.dateOfExpiry)
    dictionary["dateOfIssue"] = dateFormatter.string(fromOptional: document.dateOfIssue)
    return dictionary
  }
  
  func toDictionary(idCard document: IdCardIdentificationResult) -> [String: Any?] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var dictionary = [String: Any]()
    dictionary["documentBackFile"] = document.documentBackImageFilePath
    dictionary["documentFrontFile"] = document.documentFrontImageFilePath
    dictionary["documentNumber"] = document.documentNumber
    dictionary["documentType"] = document.documentType
    dictionary["givenNames"] = document.givenNames
    dictionary["isEighteenPlus"] = document.isEighteenPlus?.boolValue
    dictionary["isSixteenPlus"] = document.isSixteenPlus?.boolValue
    dictionary["isTwentyOnePlus"] = document.isTwentyOnePlus?.boolValue
    dictionary["issuingCountry"] = document.issuingCountry
    dictionary["nationality"] = document.nationality
    dictionary["sex"] = document.sex
    dictionary["surname"] = document.surname
    dictionary["dateOfBirth"] = dateFormatter.string(fromOptional: document.dateOfBirth)
    dictionary["dateOfExpiry"] = dateFormatter.string(fromOptional: document.dateOfExpiry)
    dictionary["dateOfIssue"] = dateFormatter.string(fromOptional: document.dateOfIssue)
    return dictionary
  }

  func toDictionary(driversLicense document: DriversLicenseIdentificationResult) -> [String: Any?] {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    var dictionary = [String: Any]()
    dictionary["documentBackFile"] = document.documentBackImageFilePath
    dictionary["documentFrontFile"] = document.documentFrontImageFilePath
    dictionary["documentNumber"] = document.documentNumber
    dictionary["documentType"] = document.documentType
    dictionary["givenNames"] = document.givenNames
    dictionary["isEighteenPlus"] = document.isEighteenPlus?.boolValue
    dictionary["isSixteenPlus"] = document.isSixteenPlus?.boolValue
    dictionary["isTwentyOnePlus"] = document.isTwentyOnePlus?.boolValue
    dictionary["issuingCountry"] = document.issuingCountry
    dictionary["surname"] = document.surname
    dictionary["dateOfBirth"] = dateFormatter.string(fromOptional: document.dateOfBirth)
    dictionary["dateOfExpiry"] = dateFormatter.string(fromOptional: document.dateOfExpiry)
    dictionary["dateOfIssue"] = dateFormatter.string(fromOptional: document.dateOfIssue)
    dictionary["jurisdiction"] = document.jurisdiction
    dictionary["licenseClass"] = document.licenseClass
    let licenseClassDetail: [String: Any]? = document.licenseClassDetails?.mapValues({ (detail) in
      var dictionaryDetail = [String: Any]()
      dictionaryDetail["from"] = dateFormatter.string(fromOptional: detail.from)
      dictionaryDetail["to"] = dateFormatter.string(fromOptional: detail.to)
      dictionaryDetail["notes"] = detail.notes
      return dictionaryDetail
    })
    dictionary["licenseClassDetails"] = licenseClassDetail
    return dictionary
  }

  func applyCustomTheme() {
    let theme = AuthenteqFlowTheme(
      primaryColor: UIColor(hex: 0x33B3CC)!,
      textColor: UIColor(hex: 0xBAC7BF)!,
      screenBackgroundColor: UIColor(hex: 0x333333)!,
      font: UIFont(name: "WorkSans-Regular", size: 1)!,
      boldFont: UIFont(name: "WorkSans-Bold", size: 1)!,
      identificationInstructionImageForSelfie: nil,
      identificationInstructionImageForPassport: nil,
      identificationInstructionImageForDriverLicense: nil,
      identificationInstructionImageForIdCard: nil
    )
    AuthenteqFlow.instance.theme = theme
  }

  func errorDescription(from error: AuthenteqFlowError) -> String {
    switch error {
    case .authenteqLicenseError:
      return "License error"
    case .userNotFound:
      return "User not found"
    case .cancelledByUser:
      return "User cancelled flow"
    case .ocrSetupError:
      return "Setup error"
    case .userDeactivated:
      return "User was deactivated"
    case .jailbreakDetected:
      return "Device jailbreak detected"
    case .internalSetupError:
      return "Setup error"
    case .livenessSetupError:
      return "Setup error"
    case .connectionCertificateError:
      return "Unsecure internet connection"
    case .invalidConfiguration:
      return "Configuration not allowed"
    }
  }

  func errorCode(from error: AuthenteqFlowError) -> String {
    switch error {
    case .authenteqLicenseError:
      return "authenteqLicenseError"
    case .userNotFound:
      return "userNotFound"
    case .cancelledByUser:
      return "userCancelled"
    case .ocrSetupError:
      return "ocrSetupError"
    case .userDeactivated:
      return "userDeactivated"
    case .jailbreakDetected:
      return "jailbreakDetected"
    case .internalSetupError:
      return "internalSetupError"
    case .livenessSetupError:
      return "livenessSetupError"
    case .connectionCertificateError:
      return "connectionCertificateError"
    case .invalidConfiguration:
      return "invalidConfiguration"
    }
  }

  func nsError(from error: AuthenteqFlowError) -> NSError {
    return NSError(
      domain: "com.authenteq",
      code: error.rawValue,
      userInfo: [NSLocalizedDescriptionKey: errorDescription(from: error)]
    )
  }

  func closeAuthenteqFlow() {
    authenteqViewController?.dismiss(animated: true)
    authenteqViewController = nil
    resolverBlock = nil
    rejecterBlock = nil
  }
}

// MARK: - AuthenteqIdentificationDelegate

extension RNAuthenteqFlow: AuthenteqIdentificationDelegate {

  func authenteqDidFinishIdentification(with result: IdentificationResult) {
    let dictionary = toDictionary(result: result)
    resolverBlock?(dictionary)
    closeAuthenteqFlow()
  }

  func authenteqDidFailIdentification(with error: AuthenteqFlowError) {
    rejecterBlock?(errorCode(from: error), errorDescription(from: error), nsError(from: error))
    closeAuthenteqFlow()
  }
}

// MARK - AuthenteqSelfieAuthentificationDelegate

extension RNAuthenteqFlow: AuthenteqSelfieAuthentificationDelegate {

  func authenteqDidFinishSelfieAuthentication(with result: Bool) {
    resolverBlock?(result)
    closeAuthenteqFlow()
  }

  func authenteqDidFailSelfieAuthentication(with error: AuthenteqFlowError) {
    rejecterBlock?(errorCode(from: error), errorDescription(from: error), nsError(from: error))
    closeAuthenteqFlow()
  }
}

// MARK: - UIColor extension

extension UIColor {

  convenience init?(hex: Int, transparency: CGFloat = 1) {
    let red = CGFloat((hex >> 16) & 0xFF)
    let green = CGFloat((hex >> 8) & 0xFF)
    let blue = CGFloat(hex & 0xFF)
    self.init(
      red: red / 255.0,
      green: green / 255.0,
      blue: blue / 255.0,
      alpha: min(max(transparency, 0), 1)
    )
  }
}

// MARK: - DateFormatter extension

extension DateFormatter {

  func string(fromOptional date: Date?) -> String? {
    if let date = date {
      return string(from: date as Date)
    } else {
      return nil
    }
  }
}
