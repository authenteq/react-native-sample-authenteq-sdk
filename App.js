import React, {Component}  from 'react';
import {
  Button,
  Clipboard,
  Image,
  View,
  StyleSheet,
  Platform,
  Text
} from 'react-native';
import AuthenteqFlow from './AuthenteqFlow';
class App extends Component {
  state = {
    license: '253441d7fd8a47a2b83788d5480bf904',
    selfieAuthenticationDisabled: true,
    userId: null,
    version: null
  }

  startIdentification = () => {
    AuthenteqFlow
      .identification(this.state.license, [['PP','DL','NID']])
      .then((res) => {
        console.log(res);
        Clipboard.setString(JSON.stringify(res))
        alert('Result copied to clipboard')
        this.setState(prevState => ({
          selfieAuthenticationDisabled: false
        }))
        this.setState(prevState => ({
          userId: res.userId
        }))
      }).catch((error) => {
        console.error(error);
      });
  }

  componentDidMount(){
    AuthenteqFlow.getVersion(version =>{
      this.setState(prevState => ({
        version: version
      }))
    });
  }

  startSelfieAuthentication = () => {
    AuthenteqFlow
      .selfieAuthentication(this.state.license, this.state.userId)
      .then((res) => {
        console.log(res);
        Clipboard.setString(JSON.stringify(res))
        alert('Result copied to clipboard')
      }).catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fffffdf9', alignItems: 'center', justifyContent: 'flex-end' }}>
        <Image style={{width: '50%', marginBottom: 100}}
          source={require('./img/authenteq_logo.png')}
          resizeMode="contain"/>
        <View style={{ width: '90%', marginBottom: 20 }} >
          <Button
            title="Identification"
            onPress={this.startIdentification} />
        </View>
        <View style={{ width: '90%', marginBottom: 20 }} >
          <Button
            disabled={this.state.selfieAuthenticationDisabled}
            title="Selfie Authentication"
            onPress={this.startSelfieAuthentication} />
        </View>
        <View style={{ marginBottom: 20 }}>
          <Text style={{width: '100%',textAlign: 'center', fontSize: 12}}>Version: {this.state.version}</Text>
        </View>
      </View>
    );
  }
  styles = StyleSheet.create({
  });

}
export default App;
