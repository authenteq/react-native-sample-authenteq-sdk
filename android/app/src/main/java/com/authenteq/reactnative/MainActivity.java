package com.authenteq.reactnative;

import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.1
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "AuthenteqSDKSample";
    }
}
