package com.authenteq.reactnative;

import android.app.Activity;
import android.content.Intent;
import android.util.SparseArray;

import com.authenteq.android.flow.result.IdentificationResult;
import com.authenteq.android.flow.result.SelfieAuthenticationResult;
import com.authenteq.android.flow.ui.identification.IdentificationActivity;
import com.authenteq.android.flow.ui.seflieauthentication.SelfieAuthenticationActivity;
import com.authenteq.android.sdk.Document;
import com.authenteq.android.sdk.DriversLicenseDocument;
import com.authenteq.android.sdk.IdCardDocument;
import com.authenteq.android.sdk.PassportDocument;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeArray;
import com.facebook.react.bridge.WritableNativeMap;

import org.threeten.bp.LocalDate;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AuthenteqFlowModule extends ReactContextBaseJavaModule {

    private static final int REQUEST_CODE_IDENTIFICATION = 1001;
    private static final int REQUEST_CODE_SELFIE_AUTHENTICATION = 1002;

    private final SparseArray<Promise> promises;

    private final BaseActivityEventListener activityEventListener = new BaseActivityEventListener() {
        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
            switch (requestCode) {
                case REQUEST_CODE_IDENTIFICATION: {
                    final Promise promise = promises.get(REQUEST_CODE_IDENTIFICATION);
                    if (resultCode == Activity.RESULT_OK) {
                        final WritableMap map = new WritableNativeMap();
                        IdentificationResult result = IdentificationActivity.getResult(data);
                        map.putString("userId", result.getUserId());
                        map.putArray("documents", createDocuments(result.getDocuments()));

                        promise.resolve(map);
                    } else {
                        final Throwable error = IdentificationActivity.getError(data);
                        if (error != null) {
                            promise.reject(error);
                        } else {
                            promise.reject("userCanceled", "User canceled flow");
                        }
                    }
                }
                break;
                case REQUEST_CODE_SELFIE_AUTHENTICATION: {
                    final Promise promise = promises.get(REQUEST_CODE_SELFIE_AUTHENTICATION);
                    if (resultCode == Activity.RESULT_OK) {
                        final WritableMap map = new WritableNativeMap();
                        final SelfieAuthenticationResult result = SelfieAuthenticationActivity.getResult(data);
                        putBoolean(map, "successful", result.isSuccessful());
                        promise.resolve(map);
                    } else {
                        final Throwable error = SelfieAuthenticationActivity.getError(data);
                        if (error != null) {
                            promise.reject(error);
                        } else {
                            promise.reject("userCanceled", "User canceled flow");
                        }
                    }
                }
                break;

            }
        }
    };

    private WritableArray createDocuments(Document[] documents) {
        final WritableArray array = new WritableNativeArray();
        for (Document document : documents) {
            if (document instanceof PassportDocument) {
                array.pushMap(createPassport(((PassportDocument) document)));
            } else if (document instanceof IdCardDocument) {
                array.pushMap(createIdCard(((IdCardDocument) document)));
            } else if (document instanceof DriversLicenseDocument) {
                array.pushMap(createDriversLicense(((DriversLicenseDocument) document)));
            }
        }
        return array;
    }

    private WritableMap createDriversLicense(DriversLicenseDocument document) {
        final WritableMap map = new WritableNativeMap();
        map.putString("givenNames", document.getGivenNames());
        map.putString("surname", document.getSurname());
        final LocalDate dateOfBirth = document.getDateOfBirth();
        map.putString("dateOfBirth", dateOfBirth == null ? null : dateOfBirth.toString());
        final LocalDate dateOfExpiry = document.getDateOfExpiry();
        map.putString("dateOfExpiry", dateOfExpiry == null ? null : dateOfExpiry.toString());
        final LocalDate dateOfIssue = document.getDateOfIssue();
        map.putString("dateOfIssue", dateOfIssue == null ? null : dateOfIssue.toString());
        map.putString("issuingCountry", document.getIssuingCountry());
        map.putString("documentNumber", document.getDocumentNumber());
        map.putString("documentType", document.getDocumentType().toString());
        map.putString("jurisdiction", document.getJurisdiction());
        map.putString("licenseClass", document.getLicenseClass());
        final Map<String, DriversLicenseDocument.LicenseClassDetails> licenseClassDetails = document.getLicenseClassDetails();
        final WritableMap licenseClassDetailsMap = new WritableNativeMap();
        if (licenseClassDetails != null) {
            for (Map.Entry<String, DriversLicenseDocument.LicenseClassDetails> entry : licenseClassDetails.entrySet()) {
                final WritableMap entryMap = new WritableNativeMap();
                final DriversLicenseDocument.LicenseClassDetails classDetails = entry.getValue();
                final LocalDate from = classDetails.getFrom();
                if (from != null) entryMap.putString("from", from.toString());
                final LocalDate to = classDetails.getTo();
                if (to != null) entryMap.putString("to", to.toString());
                final String notes = classDetails.getNotes();
                if (notes != null) entryMap.putString("notes", notes);
                licenseClassDetailsMap.putMap(entry.getKey(), entryMap);
            }
        }
        map.putMap("licenseClassDetails", licenseClassDetailsMap);
        putBoolean(map, "isSixteenPlus", document.isSixteenPlus());
        putBoolean(map, "isEighteenPlus", document.isEighteenPlus());
        putBoolean(map, "isTwentyOnePlus", document.isTwentyOnePlus());
        putFile(map, "documentFrontFile", document.getImageFrontFile());
        putFile(map, "documentBackFile", document.getImageBackFile());
        return map;

    }

    private WritableMap createIdCard(IdCardDocument document) {
        final WritableMap map = new WritableNativeMap();
        map.putString("givenNames", document.getGivenNames());
        map.putString("surname", document.getSurname());
        final LocalDate dateOfBirth = document.getDateOfBirth();
        map.putString("dateOfBirth", dateOfBirth == null ? null : dateOfBirth.toString());
        final LocalDate dateOfExpiry = document.getDateOfExpiry();
        map.putString("dateOfExpiry", dateOfExpiry == null ? null : dateOfExpiry.toString());
        final LocalDate dateOfIssue = document.getDateOfIssue();
        map.putString("dateOfIssue", dateOfIssue == null ? null : dateOfIssue.toString());
        map.putString("sex", document.getSex() == null ? null : document.getSex().toString());
        map.putString("nationality", document.getNationality());
        map.putString("issuingCountry", document.getIssuingCountry());
        map.putString("documentNumber", document.getDocumentNumber());
        map.putString("documentType", document.getDocumentType().toString());
        putBoolean(map, "isSixteenPlus", document.isSixteenPlus());
        putBoolean(map, "isEighteenPlus", document.isEighteenPlus());
        putBoolean(map, "isTwentyOnePlus", document.isTwentyOnePlus());
        putFile(map, "documentFrontFile", document.getImageFrontFile());
        putFile(map, "documentBackFile", document.getImageBackFile());
        return map;
    }

    private WritableMap createPassport(PassportDocument document) {
        final WritableMap map = new WritableNativeMap();
        map.putString("givenNames", document.getGivenNames());
        map.putString("surname", document.getSurname());
        final LocalDate dateOfBirth = document.getDateOfBirth();
        map.putString("dateOfBirth", dateOfBirth == null ? null : dateOfBirth.toString());
        final LocalDate dateOfExpiry = document.getDateOfExpiry();
        map.putString("dateOfExpiry", dateOfExpiry == null ? null : dateOfExpiry.toString());
        final LocalDate dateOfIssue = document.getDateOfIssue();
        map.putString("dateOfIssue", dateOfIssue == null ? null : dateOfIssue.toString());
        map.putString("sex", document.getSex() == null ? null : document.getSex().toString());
        map.putString("nationality", document.getNationality());
        map.putString("issuingCountry", document.getIssuingCountry());
        map.putString("documentNumber", document.getDocumentNumber());
        map.putString("documentType", document.getDocumentType().toString());
        putBoolean(map, "isSixteenPlus", document.isSixteenPlus());
        putBoolean(map, "isEighteenPlus", document.isEighteenPlus());
        putBoolean(map, "isTwentyOnePlus", document.isTwentyOnePlus());
        putFile(map, "documentFrontFile", document.getImageFile());
        return map;
    }

    public AuthenteqFlowModule(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(activityEventListener);
        promises = new SparseArray<>();
    }

    private void putBoolean(@NonNull WritableMap map, @NonNull String key, @Nullable Boolean value) {
        if (value == null) {
            map.putNull(key);
        } else {
            map.putBoolean(key, value);
        }
    }

    private void putFile(@NonNull WritableMap map, @NonNull String key, @Nullable File value) {
        if (value == null) {
            map.putNull(key);
        } else {
            map.putString(key, value.getAbsolutePath());
        }
    }

    @Override
    public String getName() {
        return "RNAuthenteqFlow";
    }


    @ReactMethod
    public void identification(String license, ReadableArray steps, Promise promise) {
        final Activity activity = getReactApplicationContext().getCurrentActivity();
        try {
            final Document.Type[][] documentTypes = convertStepsToDocumentTypes(steps);
            IdentificationActivity.startForResult(activity, REQUEST_CODE_IDENTIFICATION, license, documentTypes, R.style.AuthenteqDark);
            promises.put(REQUEST_CODE_IDENTIFICATION, promise);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    private Document.Type[][] convertStepsToDocumentTypes(ReadableArray strings) {
        final Document.Type[][] result = new Document.Type[strings.size()][];
        for (int i = 0; i < strings.size(); i++) {
            ReadableArray steps = strings.getArray(i);
            final List<Document.Type> resultTypes = new ArrayList<>();
            for (int j = 0; j < steps.size(); j++) {
                final String stepDocumentType = steps.getString(j);
                switch (stepDocumentType) {
                    case "NID":
                        resultTypes.add(Document.Type.NATIONAL_ID);
                        break;
                    case "PP":
                        resultTypes.add(Document.Type.PASSPORT);
                        break;
                    case "DL":
                        resultTypes.add(Document.Type.DRIVERS_LICENSE);
                        break;
                }
            }
            result[i] = resultTypes.toArray(new Document.Type[0]);
        }
        return result;
    }

    @ReactMethod
    public void selfieAuthentication(String license, String userId, Promise promise) {
        final Activity activity = getReactApplicationContext().getCurrentActivity();
        try {
            SelfieAuthenticationActivity.startForResult(activity, REQUEST_CODE_SELFIE_AUTHENTICATION, license, userId, R.style.AuthenteqDark);
            promises.put(REQUEST_CODE_SELFIE_AUTHENTICATION, promise);
        } catch (Exception e) {
            promise.reject(e);
        }
    }

    @ReactMethod
    public void getVersion(Callback callback) {
        callback.invoke(BuildConfig.VERSION_NAME);
    }


}